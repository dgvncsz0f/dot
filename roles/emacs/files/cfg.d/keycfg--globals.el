;; editing/visual
(global-set-key (kbd "<f6>") 'whitespace-mode)
(global-set-key (kbd "<f7>") 'visual-line-mode)
(global-set-key (kbd "S-<f6>") 'column-highlight-mode)
(global-set-key (kbd "S-<f7>") 'toggle-truncate-lines)
(global-set-key (kbd "C-S-r") 'revert-buffer)
(global-set-key (kbd "C-M-k") 'kill-whole-line)
(global-set-key (kbd "C-c <C-return>") 'rectangle-mark-mode)
(global-set-key (kbd "C-c ro") 'open-rectangle)

;; company
(global-set-key (kbd "M-TAB") 'company-complete)

;; project
(global-set-key (kbd "C-c p m s") 'magit-status)
(global-set-key (kbd "C-c p m t") 'git-timemachine)

;; ivy/counsel
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-s") 'swiper)
(global-set-key (kbd "C-x b") 'counsel-switch-buffer)
(global-set-key (kbd "C-x p g") 'counsel-bookmark)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "C-c p s a") 'counsel-projectile-rg)
(global-set-key (kbd "C-c p f") 'counsel-projectile-find-file)
(global-set-key (kbd "C-c p d") 'counsel-projectile-find-dir)
(global-set-key (kbd "C-h f") 'counsel-describe-function)
(global-set-key (kbd "C-h v") 'counsel-describe-variable)
(global-set-key (kbd "C-h l") 'counsel-find-library)
(global-set-key (kbd "C-h i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "C-x 8 RET") 'counsel-unicode-char)

;; evil-numbers
(global-set-key (kbd "C-c +") 'evil-numbers/inc-at-pt)
(global-set-key (kbd "C-c -") 'evil-numbers/dec-at-pt)

;; expand-region
(global-set-key (kbd "C-= C-'") 'er/mark-inside-quotes)
(global-set-key (kbd "C-= C-u") 'er/mark-url)
(global-set-key (kbd "C-= C-=") 'er/expand-region)
(global-set-key (kbd "C-= C-9") 'er/mark-inside-pairs)

;; spell-checker
(global-set-key (kbd "<f5>") 'ispell-change-dictionary)
(global-set-key (kbd "S-<f5>") 'dx15/toggle-flyspell)

;; zoom
(global-set-key (kbd "C-c z") 'zoom)

;; avy-char
(global-set-key (kbd "C-<return> l") 'avy-goto-line)
(global-set-key (kbd "C-<return> w") 'avy-goto-word-1)
(global-set-key (kbd "C-<return> c") 'avy-goto-char)
(global-set-key (kbd "C-<return> z") 'avy-zap-up-to-char)

;; switch-window
(global-set-key (kbd "C-x o") 'switch-window)
(global-set-key (kbd "C-x 1") 'switch-window-then-maximize)
(global-set-key (kbd "C-x 2") 'switch-window-then-split-below)
(global-set-key (kbd "C-x 3") 'switch-window-then-split-right)
(global-set-key (kbd "C-x 0") 'switch-window-then-delete)

;; highlight-symbol
(global-set-key [(control f3)] 'highlight-symbol)
(global-set-key [f3] 'highlight-symbol-next)
(global-set-key [(shift f3)] 'highlight-symbol-prev)
(global-set-key [(meta f3)] 'highlight-symbol-query-replace)

;; eshell
(global-set-key (kbd "C-c s") 'eshell)

;; dx15
(global-set-key (kbd "C-c C-e") 'dx15/eval-and-replace)
(global-set-key (kbd "C-S-o") 'dx15/prepend-line)
(global-set-key (kbd "C-o") 'dx15/append-line)
