(require 'elixir-mode)
(require 'projectile)
(require 'lsp)

(add-to-list 'exec-path "/Users/dsouza/dev/dsouza/other/elixir-ls/release")

(add-hook 'elixir-mode-hook
          (lambda ()
            (turn-on-smartparens-mode)
            (show-paren-mode t)
            (display-line-numbers-mode t)
            (lsp)))
