;; -*- mode: emacs-lisp; -*-

(setq erc-hide-list '("JOIN" "PART" "QUIT" "AWAY"))
(setq erc-track-exclude-types '("MODE" "AWAY"))
(setq erc-auto-query 'bury)
