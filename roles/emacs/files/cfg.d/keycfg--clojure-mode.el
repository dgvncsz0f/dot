(require 'clojure-mode)

(define-key clojure-mode-map (kbd "C-c M-q") 'dx15_autofmt/format-clojure-buffer)
