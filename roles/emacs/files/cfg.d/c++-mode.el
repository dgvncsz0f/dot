(add-hook 'c++-mode-hook
          (lambda ()
            (display-line-numbers-mode t)
            (c-set-style "dgvncsz0f")))
