(require 'python)

(define-key python-mode-map (kbd "C-c M-q") 'dx15_autofmt/format-python-buffer)
