(add-hook 'python-mode-hook
          (lambda ()
            (display-line-numbers-mode t)
            (modify-syntax-entry ?_ "_")))
