(require 'haskell)

(define-key haskell-mode-map (kbd "C-c M-q") 'dx15_autofmt/format-haskell-buffer)
