(require 'flycheck-clj-kondo)

(add-hook 'clojure-mode-hook
          (lambda ()
            (setq cider-prefer-local-resources t
                  cider-repl-pop-to-buffer-on-connect nil)
            (dolist (checker '(clj-kondo-clj clj-kondo-cljs clj-kondo-cljc clj-kondo-edn))
              (setq flycheck-checkers (cons checker (delq checker flycheck-checkers))))
            (eldoc-mode)
            (cider-mode)
            (flycheck-mode)
            (turn-on-smartparens-strict-mode)
            (show-paren-mode t)
            (display-line-numbers-mode t)
            (cider-mode)))
