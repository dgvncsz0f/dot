(require 'lsp-java)

(autoload 'google-set-c-style "google-c-style")
(autoload 'google-make-newline-indent "google-c-style")

(add-hook 'java-mode-hook
          (lambda ()
            (setq c-basic-offset 2)
            (google-set-c-style)
            (google-make-newline-indent)
            (display-line-numbers-mode)
            (setq lsp-java-java-path (concat (shell-command-to-string "bash -l -c 'sdk home java current'") "/bin/java"))
            (setq lsp-java-vmargs (list "-noverify" "-Xmx4G" "-XX:+UseG1GC" "-XX:+UseStringDeduplication" (concat "-javaagent:" (expand-file-name "~/.emacs.d/java/lombok.jar"))))
            (setq lsp-ui-sideline-enable nil)
            (setq lsp-ui-doc-enable nil)
            (setq lsp-ui-peek-enable nil)
            (lsp)))
