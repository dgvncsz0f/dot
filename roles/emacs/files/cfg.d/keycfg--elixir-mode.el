(require 'elixir-mode)

(define-key elixir-mode-map (kbd "C-c C-f") 'elixir-format)
(define-key elixir-mode-map (kbd "C-<return> t") 'lsp-avy-lens)
