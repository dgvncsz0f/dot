(require 'lsp)
(require 'lsp-haskell)

(add-hook 'haskell-mode-hook #'lsp)
(add-hook 'haskell-mode-hook
          (lambda ()
            (turn-on-haskell-indentation)
            (turn-on-smartparens-mode)
            (show-paren-mode t)
            (display-line-numbers-mode t)))
(add-hook 'haskell-literate-mode-hook #'lsp)
