(require 'rust-mode)

(add-hook 'rust-mode-hook
          (lambda ()
            (setq rust-indent-offset 4)
            (display-line-numbers-mode t)
            (lsp)))

(define-key rust-mode-map (kbd "C-<return> t") 'lsp-avy-lens)
