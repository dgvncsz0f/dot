; -*- mode: emacs-lisp; -*-

(defun dx15_autofmt/format-clojure-buffer ()
  (interactive)
  (cider-format-buffer))

(defun dx15_autofmt/format-haskell-buffer ()
  (interactive)
  (let ((haskell-mode-stylish-haskell-path (string-trim (shell-command-to-string "stack exec which stylish-haskell"))))
    (haskell-mode-stylish-buffer)))

(defun dx15_autofmt/format-python-buffer ()
  (interactive)
  (basic-save-buffer)
  (shell-command (concat "black " buffer-file-name)))

(provide 'dx15_autofmt)
