; -*- mode: emacs-lisp; -*-

(require 'dx15_emacswiki)

{% for item in emacswiki.requires %}
(require '{{ item }})
{% endfor %}
