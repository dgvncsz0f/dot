; -*- mode: emacs-lisp; -*-

(require 'custom)
(require 'server)

(set-face-attribute 'default nil :font "Iosevka SS07" :height {{ emacs_font_height }})
(setq default-text-properties '(line-spacing 0.15 line-height 1.15))

(setq dx15-root "{{ root }}/.emacs.d/dx15")
(setq dx15-libd "{{ root }}/.emacs.d/dx15/lib.d")
(setq dx15-exed "{{ root }}/.emacs.d/dx15/exe.d")
(setq dx15-files "{{ root }}/.emacs.d/dx15/files")
(setq user-emacs-directory "{{ root }}/.emacs.d/")
(setq custom-file "{{ root }}/.emacs.d/custom-file")

(add-to-list 'load-path dx15-libd)

(mapc 'load (file-expand-wildcards (concat dx15-exed "/*.el")))

(when (file-exists-p custom-file) (load custom-file))

(unless (server-running-p) (server-start))
