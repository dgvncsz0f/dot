;; -*- mode: lisp -*-

(ql:quickload :swank)
(ql:quickload :clx-truetype)

(in-package :stumpwm)

(load-module :ttf-fonts)
(load-module :battery-portable)

;; use super-q
(set-prefix-key (kbd "s-q"))

;; aliases

(setf *window-format* "%m%n%s%20c")
(setf *time-modeline-string* "%e, %a %l:%M")
(setf *mode-line-timeout* 10)
(setf *mode-line-border-width* 0)

(setf *screen-mode-line-format*
      (list "%g %W ^> | %B"))

;; commands

(defcommand dx15-runcmd () ()
            "exec a command"
            (run-shell-command "bash -l -c 'rofi -show run'"))
(define-key *top-map* (kbd "s-r") "dx15-runcmd")

(defcommand dx15-editor () ()
           "run or raise emacs"
           (run-or-raise "bash -l -c dx15-editor" '(:class "Emacs")))
(define-key *top-map* (kbd "s-e") "dx15-editor")
(define-key *root-map* (kbd "e") "dx15-editor")
(undefine-key *root-map* (kbd "C-e"))

(defcommand dx15-terminal () ()
            "run or raise terminal"
            (run-or-raise "gnome-terminal" '(:class "Gnome-terminal")))
(define-key *top-map* (kbd "s-t") "dx15-terminal")
(define-key *root-map* (kbd "c") "dx15-terminal")
(undefine-key *root-map* (kbd "C-c"))

(defcommand dx15-browser () ()
  "run or raise browser"
  (run-or-raise "firefox" '(:class "firefox")))
(define-key *top-map* (kbd "s-b") "dx15-browser")

(defcommand dx15-passmenu () ()
  "run passmenu"
  (run-shell-command "bash -l -c passmenu"))
(define-key *top-map* (kbd "s-p") "dx15-passmenu")

(defcommand dx15-windmenu () ()
  "run rofi -show window"
  (run-shell-command "rofi -show window"))
(define-key *top-map* (kbd "s-w") "dx15-windmenu")

(defcommand dx15-lock () ()
  "lock computer"
  (run-shell-command "xscreensaver-command -lock"))
(define-key *top-map* (kbd "s-l") "dx15-lock")

;; defaults

(xft:cache-fonts)
(set-font
 (make-instance 'xft:font :family "Iosevka SS07" :subfamily "Regular" :size 11))

(setf *startup-message* nil)
(setq *ignore-wm-inc-hints* t)
(setq *window-border-style* :thin)
(setf *mouse-focus-policy* :click)

(loop
   :for head in (screen-heads (current-screen))
   :do (enable-mode-line (current-screen) head t))

;; swank server
;; (funcall (intern "CREATE-SERVER" :swank) :port 19876)
